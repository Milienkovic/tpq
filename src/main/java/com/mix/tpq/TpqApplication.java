package com.mix.tpq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;
import java.security.SecureRandom;

@SpringBootApplication
public class TpqApplication {


    public static void main(String[] args) {

        SpringApplication.run(TpqApplication.class, args);

    }

}
