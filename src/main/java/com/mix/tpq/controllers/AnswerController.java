package com.mix.tpq.controllers;

import com.mix.tpq.models.payload.AnswerDTO;
import com.mix.tpq.services.data.AnswerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AnswerController {

    private final AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/questions/{questionId}/answers/new")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView newAnswer(@PathVariable int questionId) {
        ModelAndView model = new ModelAndView();
        AnswerDTO answer = new AnswerDTO();
        model.addObject("answer", answer);
        model.setViewName("createAnswer");
        return model;
    }

    @PostMapping("/questions/{questionId}/answers/create")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView createAnswer(@PathVariable int questionId,
                                     @Valid @ModelAttribute("answer") AnswerDTO answer,
                                     BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        if (answerService.existsByAnswerText(answer.getAnswer(), answer.getId(), questionId)) {
            bindingResult.rejectValue("answer", "error.answer", "This question has this answer already ");
        }
        if (answerService.hasCorrectAnswer(questionId, answer.getId(), answer.isCorrect())) {
            bindingResult.rejectValue("correct", "error.correct", "This question has correct answer already");
        }

        if (bindingResult.hasErrors()) {
            model.addObject("answer", answer);
            if (answer.getId() == 0) {
                model.setViewName("createAnswer");
            } else {
                model.setViewName("redirect:/answers/" + answer.getId() + "/edit");
            }
            return model;
        }
        answerService.save(questionId, answer);
        model.setViewName("redirect:/quiz/" + answer.getQuestion().getQuiz().getId() + "/questions/new");
        return model;
    }

    @GetMapping("/answers/{answerId}/edit")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView editAnswer(@PathVariable int answerId) {
        AnswerDTO answer = answerService.findById(answerId);
        ModelAndView model = new ModelAndView();
        model.addObject("answer", answer);
        model.setViewName("editAnswer");
        return model;
    }

    @PostMapping("/answers/{answerId}/delete")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView deleteAnswer(@PathVariable int answerId, ModelAndView model) {
        AnswerDTO answer = answerService.findById(answerId);
        answerService.deleteById(answerId);
        model.setViewName("redirect:/quiz/" + answer.getQuestion().getQuiz().getId() + "/questions/new");
        return model;
    }
}
