package com.mix.tpq.controllers;

import com.mix.tpq.models.payload.RegisterUser;
import com.mix.tpq.services.data.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AuthController {

    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public ModelAndView register (){
        ModelAndView model = new ModelAndView();
        RegisterUser user = new RegisterUser();
        model.addObject("user", user);
        model.setViewName("registration");
        return model;
    }
    @PostMapping("/registration")
    public ModelAndView registerNewUser (@Valid @ModelAttribute("user") RegisterUser user, BindingResult bindingResult){
        ModelAndView model = new ModelAndView();
        if (userService.existsByEmail(user.getEmail())){
            bindingResult.rejectValue("email","error.user", "email is not unique");
        }
        if (userService.existsByUsername(user.getUsername())){
            bindingResult.rejectValue("username", "error.user","username is not unique");
        }
        if (bindingResult.hasErrors()){
            model.addObject("user",user);
            model.setViewName("registration");
            return model;
        }
        userService.save(user);
        model.setViewName("redirect:/login");
        return model;
    }
    @GetMapping("/login")
    public ModelAndView login (){
        ModelAndView model = new ModelAndView();
        model.setViewName("login");
        return model;
    }
}
