package com.mix.tpq.controllers;

import com.mix.tpq.models.payload.QuestionDTO;
import com.mix.tpq.models.payload.QuestionStatistic;
import com.mix.tpq.services.data.QuestionService;
import com.mix.tpq.services.data.QuizService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
public class QuestionController {
    private final QuestionService questionService;
    private final QuizService quizService;

    public QuestionController(QuestionService questionService, QuizService quizService) {
        this.questionService = questionService;
        this.quizService = quizService;
    }

    @GetMapping("/quiz/{quizId}/questions/new")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView newQuestion(@PathVariable int quizId) {
        ModelAndView model = new ModelAndView();
        QuestionDTO question = new QuestionDTO();
        Set<QuestionDTO> questions = questionService.findAll();
        model.addObject("question", question);
        model.addObject("questions", questions);
        model.setViewName("createQuestion");
        return model;
    }

    @GetMapping("/questions/{questionId}/edit")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView editQuestion(@PathVariable int questionId) {
        QuestionDTO question = questionService.findById(questionId);
        ModelAndView model = new ModelAndView();
        model.addObject("question", question);
        model.setViewName("editQuestion");
        return model;
    }

    @PostMapping("/quiz/{quizId}/questions/create")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView createQuestion(@PathVariable int quizId, @Valid @ModelAttribute("question") QuestionDTO question, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        if (questionService.existsByQuestionText(question.getQuestion(), question.getId())) {
            bindingResult.rejectValue("question", "error.question", "This question already exist");
        }
        if (bindingResult.hasErrors()) {
            model.addObject("question", question);
            if (question.getId() == 0) {
                model.setViewName("createQuestion");
            } else {
                model.setViewName("redirect:/questions/" + question.getId() + "/edit");

            }
            return model;
        }
        questionService.saveQuestion(question, quizId);
        model.setViewName("redirect:/quiz/" + quizId + "/questions/new");
        return model;
    }

    @PostMapping("/questions/{questionId}/delete")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView deleteQuestion(@PathVariable int questionId) {
        QuestionDTO question = questionService.findById(questionId);
        questionService.deleteById(questionId);
        ModelAndView model = new ModelAndView();
        model.setViewName("redirect:/quiz/" + question.getQuiz().getId() + "/questions/new");
        return model;
    }

    @GetMapping("/questions")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView getQuestions() {
        ModelAndView model = new ModelAndView();
        List<QuestionDTO> questions = questionService.findAllSortedByPercentageDesc();
        model.addObject("questions", questions);
        model.setViewName("questions");
        return model;
    }

    @GetMapping("/questions/worst")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ModelAndView getQuestionsWithWorstAnswers() {
        ModelAndView model = new ModelAndView();
        List<QuestionStatistic> questions = questionService.getQuestionsWithWorstAnswer();
        model.addObject("questions", questions);
        model.setViewName("questionsWithWorstAnswer");
        return model;
    }
}
