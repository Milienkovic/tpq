package com.mix.tpq.controllers;

import com.mix.tpq.models.payload.*;
import com.mix.tpq.services.data.QuestionService;
import com.mix.tpq.services.data.QuizService;
import com.mix.tpq.services.data.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@Controller
public class QuizController {
    private final QuizService quizService;
    private final QuestionService questionService;
    private final UserService userService;

    public QuizController(QuizService quizService, QuestionService questionService, UserService userService) {
        this.quizService = quizService;
        this.questionService = questionService;
        this.userService = userService;
    }

    @GetMapping({"/", "/quiz"})
    public ModelAndView home() {
        ModelAndView model = new ModelAndView();
        boolean isQuizExists = quizService.isQuizExists();
        if (isQuizExists) {
            QuizDTO dbQuiz = quizService.findAll().get(0);
            model.addObject("quiz", dbQuiz);
        }
        model.addObject("isQuizExists", isQuizExists);
        model.setViewName("quiz");
        return model;
    }

    @GetMapping("/quiz/create")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView createQuiz() {
        ModelAndView model = new ModelAndView();
        QuizDTO quiz = new QuizDTO();
        model.addObject("quiz", quiz);
        model.setViewName("createQuiz");
        return model;
    }

    @PostMapping("/quiz/create")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView createQuiz(@Valid @ModelAttribute("quiz") QuizDTO quiz, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        if (bindingResult.hasErrors()) {
            model.addObject("quiz", quiz);
            model.setViewName("createQuiz");
            return model;
        }
        quizService.saveQuiz(quiz);
        model.setViewName("redirect:/quiz");
        return model;
    }

    @GetMapping("/quiz/{quizId}/edit")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView editQuiz(@PathVariable int quizId) {
        QuizDTO dbQuiz = quizService.findById(quizId);
        ModelAndView model = new ModelAndView();
        model.addObject("quiz", dbQuiz);
        model.setViewName("editQuiz");
        return model;
    }

    @PostMapping("/quiz/{quizId}/delete")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView deleteQuiz(@PathVariable int quizId) {
        quizService.deleteQuiz(quizId);
        userService.updateAll();
        ModelAndView model = new ModelAndView();
        model.setViewName("redirect:/quiz");
        return model;
    }

    @GetMapping("/quiz/{quizId}/start")
    @PreAuthorize("hasAuthority('USER')")
    public ModelAndView playQuiz(@PathVariable int quizId, HttpSession session) {
        ModelAndView model = new ModelAndView();
        QuizScore quizScore = new QuizScore(quizService.findById(quizId).getQuestions());
        session.setAttribute("quizScore", quizScore);
        QuestionDTO question = quizScore.getQuestions().get(quizScore.getIndex());
        model.addObject("question", question);
        model.setViewName("playQuiz");
        return model;
    }

    @PostMapping("/quiz/{quizId}/next")
    @PreAuthorize("hasAuthority('USER')")
    public ModelAndView nextQuestion(@PathVariable int quizId,
                                     @ModelAttribute QuestionDTO question,
                                     ModelAndView model,
                                     HttpSession session) {
        if (session.getAttribute("quizScore") != null) {
            QuizScore quizScore = (QuizScore) session.getAttribute("quizScore");
            quizScore = questionService.updateQuizScore(question, quizScore);
            session.setAttribute("quizScore", quizScore);
            log.error(question + "");
            log.error("quiz score ---------------------------------------> \n" + quizScore + "");
            QuestionDTO q = quizScore.getQuestions().get(quizScore.getIndex());
            model.addObject("question", q);
        }
        model.setViewName("playQuiz");
        return model;
    }

    @PostMapping("/quiz/{quizId}/skip")
    @PreAuthorize("hasAuthority('USER')")
    public ModelAndView skipQuestion(@PathVariable int quizId,
                                     @ModelAttribute QuestionDTO question,
                                     ModelAndView model,
                                     HttpSession session) {

        if (session.getAttribute("quizScore") != null) {
            QuizScore quizScore = (QuizScore) session.getAttribute("quizScore");
            quizScore = questionService.updateQuizScoreAfterSkip(question, quizScore);
            session.setAttribute("quizScore", quizScore);
            log.error("quiz score skip ---------------------------------------> \n" + quizScore + "");
            QuestionDTO q = quizScore.getQuestions().get(quizScore.getIndex());
            model.addObject("question", q);
        }
        model.setViewName("playQuiz");
        return model;
    }

    @PostMapping("/quiz/{quizId}/cancel")
    @PreAuthorize("hasAuthority('USER')")
    public ModelAndView cancelQuiz(@PathVariable int quizId,
                                   @ModelAttribute QuestionDTO question,
                                   ModelAndView model,
                                   HttpSession session,
                                   Principal principal) {

        if (session.getAttribute("quizScore") != null) {
            QuizScore quizScore = (QuizScore) session.getAttribute("quizScore");
            quizScore = questionService.updateQuizScoreAfterCancel(question, quizScore);
            session.setAttribute("quizScore", quizScore);
            questionService.saveAll(quizScore.getQuestions());
            userService.update(quizScore.getUserScore(), principal.getName());
            List<UserDTO> users = userService.findTop5ByCorrectAnswers();
            model.addObject("users", users);
        }
        model.setViewName("topUsers");
        return model;
    }

    @PostMapping("/quiz/{quizId}/finish")
    @PreAuthorize("hasAuthority('USER')")
    public ModelAndView finishQuiz(@PathVariable int quizId,
                                   @ModelAttribute QuestionDTO question,
                                   ModelAndView model,
                                   HttpSession session,
                                   Principal principal) {

        if (session.getAttribute("quizScore") != null) {
            QuizScore quizScore = (QuizScore) session.getAttribute("quizScore");
            quizScore = questionService.updateQuizScore(question, quizScore);
            session.setAttribute("quizScore", quizScore);
            questionService.saveAll(quizScore.getQuestions());
            userService.update(quizScore.getUserScore(), principal.getName());
            List<UserDTO> users = userService.findTop5ByCorrectAnswers();
            model.addObject("users", users);

        }
        model.setViewName("redirect:/quiz/" + quizId + "/finish");
        return model;
    }

    @GetMapping("/quiz/{quizId}/finish")
    @PreAuthorize("hasAuthority('USER')")
    public ModelAndView topUsers(@PathVariable int quizId, ModelAndView model, Principal principal) {
        List<UserDTO> users = userService.findTop5ByCorrectAnswers();
        boolean isTopPlayer = userService.isTopPlayer(principal.getName());
        model.addObject("users", users);
        model.addObject("isTopPlayer",isTopPlayer);
        model.setViewName("topUsers");
        return model;
    }


}
