package com.mix.tpq.controllers;

import com.mix.tpq.models.payload.UserDTO;
import com.mix.tpq.services.data.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Set;

@Controller
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping("/users")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView getUsers(){
        ModelAndView model = new ModelAndView();
        Set<UserDTO> users = userService.findAll();
        model.addObject("users",users);
        model.setViewName("users");
        return model;
    }
    @GetMapping("/users/sorted")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView sortedUsers(){
        ModelAndView model = new ModelAndView();
        List<UserDTO> users = userService.findAllSortedByScoreDesc();
        model.addObject("users",users);
        model.setViewName("sortedUsers");
        return model;
    }
    @GetMapping("/users/allAnswers")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView allAnswersUsers(){
        ModelAndView model = new ModelAndView();
        List<UserDTO> users = userService.usersWithAllAnswers();
        model.addObject("users",users);
        model.setViewName("sortedUsers");
        return model;
    }
    @GetMapping("/users/allCorrectAnswers")
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    public ModelAndView allCorrectAnswersUsers(){
        ModelAndView model = new ModelAndView();
        List<UserDTO> users = userService.usersWithAllCorrectAnswers();
        model.addObject("users",users);
        model.setViewName("sortedUsers");
        return model;
    }
}
