package com.mix.tpq.models.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity

public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String answer;
    @Column
    private boolean correct;
    @Column(name = "correctly_answered_counter")
    private int correctlyAnsweredCounter;
    @Column
    private int takes;
    @Column
    private double percentage;
    @ManyToOne
    @JoinColumn(name = "question_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Question question;

}
