package com.mix.tpq.models.payload;

import com.mix.tpq.models.entities.Question;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Data
public class AnswerDTO {
    private int id;
    @NotEmpty(message = "This field cannot be empty")
    private String answer;
    private boolean correct;
    private int correctlyAnsweredCounter;
    private int takes;
    private double percentage;
    private boolean userAnswer;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Question question;
}
