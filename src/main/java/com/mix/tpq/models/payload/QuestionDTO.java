package com.mix.tpq.models.payload;

import com.mix.tpq.models.entities.Answer;
import com.mix.tpq.models.entities.Quiz;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class QuestionDTO {
    private int id;
    @NotEmpty(message = "Field cannot be empty")
    private String question;
    private int correctlyAnsweredCounter;
    private int takes;
    private double percentage;
//    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    List<AnswerDTO> answers;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Quiz quiz;
}
