package com.mix.tpq.models.payload;

import lombok.Data;
@Data
public class QuestionStatistic {
    private String questionText;
    private double answerPercentage;
    private String answerText;
}
