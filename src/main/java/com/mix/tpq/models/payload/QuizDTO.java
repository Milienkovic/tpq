package com.mix.tpq.models.payload;

import com.mix.tpq.models.entities.Question;
import com.mix.tpq.models.entities.User;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class QuizDTO {

    private int id;
    @NotEmpty(message = "Field cannot be empty")
    private String name;
    List<QuestionDTO> questions;
    List<User> users;
}
