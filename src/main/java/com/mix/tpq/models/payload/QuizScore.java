package com.mix.tpq.models.payload;

import lombok.Data;

import java.util.List;

@Data
public class QuizScore {

    public QuizScore() {
    }

    public QuizScore(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    private int index;
    private UserScore userScore = new UserScore();
    private List<QuestionDTO> questions;
}
