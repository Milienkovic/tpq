package com.mix.tpq.models.payload;

import com.mix.tpq.services.validators.Email;
import com.mix.tpq.services.validators.Password;
import com.mix.tpq.services.validators.PersonsName;
import com.mix.tpq.services.validators.Username;
import lombok.Data;

@Data
public class RegisterUser {

    @PersonsName
    private String firstName;
    @PersonsName
    private String lastName;
    @Username
    private String username;
    @Email
    private String email;
    @Password
    private String password;
}
