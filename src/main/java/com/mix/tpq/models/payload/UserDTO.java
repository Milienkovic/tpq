package com.mix.tpq.models.payload;

import com.mix.tpq.services.validators.Email;
import com.mix.tpq.services.validators.PersonsName;
import com.mix.tpq.services.validators.Username;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserDTO {

    private int id;
    @PersonsName
    private String firstName;
    @PersonsName
    private String lastName;
    @Username
    private String username;
    @Email
    private String email;
    private Timestamp lastVisited;
    private int correctAnswers;
    private int numberOfAnswers;
    private boolean canceled;
}
