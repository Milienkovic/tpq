package com.mix.tpq.models.payload;

import lombok.Data;

@Data
public class UserScore {
    private int correctAnswers;
    private int numberOfAnswers;
    private boolean canceled;
}
