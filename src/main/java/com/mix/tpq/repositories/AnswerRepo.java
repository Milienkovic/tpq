package com.mix.tpq.repositories;

import com.mix.tpq.models.entities.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnswerRepo extends JpaRepository<Answer,Integer> {
    List<Answer> findAll();
    Optional<Answer> findFirstByOrderByPercentageDesc();
    Optional<Answer> findById(int id);
    Optional<Answer> findByAnswer(String text);
    boolean existsByAnswer(String text);
}
