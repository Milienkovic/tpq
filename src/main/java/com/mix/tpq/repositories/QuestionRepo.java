package com.mix.tpq.repositories;

import com.mix.tpq.models.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface QuestionRepo extends JpaRepository<Question, Integer> {
    Optional<Question> findById(int id);
    List<Question> findAll();
    List<Question> findAllByOrderByPercentageDesc();
    boolean existsByQuestion(String text);
    Optional<Question> findByQuestion(String text);
}
