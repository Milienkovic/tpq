package com.mix.tpq.repositories;

import com.mix.tpq.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
    List<User> findAllByOrderByCorrectAnswersDesc();
    List<User> findTop5ByOrderByCorrectAnswersDesc();
    boolean existsByUsername(String username);
    boolean existsByEmail(String email);
    Optional<User> findTopByOrderByCorrectAnswers();

}
