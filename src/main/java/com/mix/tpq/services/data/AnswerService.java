package com.mix.tpq.services.data;

import com.mix.tpq.models.payload.AnswerDTO;

import java.util.Set;

public interface AnswerService {
    AnswerDTO findById(int answerId);
    Set<AnswerDTO> findAll();
    AnswerDTO save(int questionId, AnswerDTO answer);
    void deleteById(int answerId);
    boolean hasCorrectAnswer(int questionId, int answerId, boolean isCorrect);
    boolean hasCorrectAnswer(int questionId);
    boolean existsByAnswerText(String text, int answerId, int questionId);
}
