package com.mix.tpq.services.data;

import com.mix.tpq.models.payload.QuestionDTO;
import com.mix.tpq.models.payload.QuestionStatistic;
import com.mix.tpq.models.payload.QuizScore;

import java.util.List;
import java.util.Set;

public interface QuestionService {
    QuestionDTO findById(int questionId);
    Set<QuestionDTO> findAll();
    QuestionDTO saveQuestion(QuestionDTO question, int quizId);
    void deleteById(int questionId);
    boolean existsByQuestionText(String text, int questionId);
    List<QuestionStatistic> getQuestionsWithWorstAnswer();
    List<QuestionDTO> findAllSortedByPercentageDesc();
    QuizScore updateQuizScore(QuestionDTO question, QuizScore quizScore);
    QuizScore updateQuizScoreAfterSkip(QuestionDTO question, QuizScore quizScore);
    QuizScore updateQuizScoreAfterCancel(QuestionDTO question, QuizScore quizScore);
    List<QuestionDTO> saveAll(List<QuestionDTO> questions);

}
