package com.mix.tpq.services.data;

import com.mix.tpq.models.payload.QuestionDTO;
import com.mix.tpq.models.payload.QuizDTO;
import com.mix.tpq.models.payload.QuizScore;

import java.util.List;

public interface QuizService {
    QuizDTO findById(int id);
    List<QuizDTO> findAll();
    QuizDTO saveQuiz(QuizDTO quiz);
    void deleteQuiz(int id);
    boolean isQuizExists();
}
