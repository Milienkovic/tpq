package com.mix.tpq.services.data;

import com.mix.tpq.models.payload.RegisterUser;
import com.mix.tpq.models.payload.UserDTO;
import com.mix.tpq.models.payload.UserScore;

import java.util.List;
import java.util.Set;

public interface UserService {
    UserDTO findById(int id);

    UserDTO findByUsername(String username);

    Set<UserDTO> findAll();

    UserDTO save(RegisterUser formUser);

    UserDTO update(UserScore userScore, String username);

    void deleteById(int id);

    List<UserDTO> findAllSortedByScoreDesc();
    List<UserDTO> findTop5ByCorrectAnswers();
    List<UserDTO> usersWithAllAnswers();
    List<UserDTO> usersWithAllCorrectAnswers();
    boolean existsByUsername(String username);
    boolean existsByEmail(String email);
    boolean isTopPlayer(String username);
    void updateAll();

}
