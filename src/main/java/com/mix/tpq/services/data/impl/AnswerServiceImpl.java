package com.mix.tpq.services.data.impl;

import com.mix.tpq.models.entities.Answer;
import com.mix.tpq.models.entities.Question;
import com.mix.tpq.models.payload.AnswerDTO;
import com.mix.tpq.repositories.AnswerRepo;
import com.mix.tpq.repositories.QuestionRepo;
import com.mix.tpq.services.data.AnswerService;
import com.mix.tpq.services.exceptions.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepo answerRepo;
    private final QuestionRepo questionRepo;
    private ModelMapper mapper;


    public AnswerServiceImpl(AnswerRepo answerRepo, QuestionRepo questionRepo) {
        this.answerRepo = answerRepo;
        this.questionRepo = questionRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public AnswerDTO findById(int answerId) {
        return mapper.map(getAnswer(answerId), AnswerDTO.class);
    }

    @Override
    public Set<AnswerDTO> findAll() {
        return answerRepo.findAll().stream().map(answer -> mapper.map(answer, AnswerDTO.class)).collect(Collectors.toSet());
    }

    @Override
    public AnswerDTO save(int questionId, AnswerDTO answer) {
        return mapper.map(persistAnswer(questionId, answer), AnswerDTO.class);
    }

    @Override
    public void deleteById(int answerId) {
        answerRepo.deleteById(answerId);
    }

    @Override
    public boolean hasCorrectAnswer(int questionId, int answerId, boolean isCorrect) {
        Question question = getQuestion(questionId);
        boolean hasCorrect = question.getAnswers().stream()
                .filter(answer -> answer.isCorrect())
                .filter(answer -> answer.getId() != answerId)
                .count() > 0;
        return isCorrect && hasCorrect;
    }

    @Override
    public boolean hasCorrectAnswer(int questionId) {
        Question question = getQuestion(questionId);
        return question.getAnswers().stream()
                .filter(answer -> answer.isCorrect())
                .count()>0;
    }

    @Override
    public boolean existsByAnswerText(String text, int answerId, int questionId) {
        Question question = getQuestion(questionId);

        boolean answerExist = question.getAnswers().stream()
                .filter(answer -> answer.getAnswer().equals(text))
                .filter(answer -> answer.getId() != answerId)
                .count() > 0;
        return answerExist;
    }

    private Answer getAnswer(int answerId) {
        return answerRepo.findById(answerId).orElseThrow(() -> new ResourceNotFoundException("Answer doesnt exist"));
    }

    private Question getQuestion(int questionId) {
        return questionRepo.findById(questionId).orElseThrow(() -> new ResourceNotFoundException("Question not found"));
    }

    private Answer persistAnswer(int questionId, AnswerDTO answer) {
//        if (answer.isCorrect() && hasCorrectAnswer(questionId)) {
//            throw new TooManyCorrectAnswersException("This question has correct answer already");
//        }
        Answer persistedAnswer;
        Question question = getQuestion(questionId);
        answer.setQuestion(question);
        if (answer.getId() == 0) {
            persistedAnswer = answerRepo.save(mapper.map(answer, Answer.class));
        } else {
            Answer dbAnswer = getAnswer(answer.getId());
            mapper.map(answer, dbAnswer);
            persistedAnswer = answerRepo.save(dbAnswer);
        }
        return persistedAnswer;
    }
}
