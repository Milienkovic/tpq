package com.mix.tpq.services.data.impl;

import com.mix.tpq.models.entities.Answer;
import com.mix.tpq.models.entities.Question;
import com.mix.tpq.models.entities.Quiz;
import com.mix.tpq.models.payload.AnswerDTO;
import com.mix.tpq.models.payload.QuestionDTO;
import com.mix.tpq.models.payload.QuestionStatistic;
import com.mix.tpq.models.payload.QuizScore;
import com.mix.tpq.repositories.QuestionRepo;
import com.mix.tpq.repositories.QuizRepo;
import com.mix.tpq.services.data.QuestionService;
import com.mix.tpq.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepo questionRepo;
    private final QuizRepo quizRepo;
    private ModelMapper mapper;

    public QuestionServiceImpl(QuestionRepo questionRepo, QuizRepo quizRepo) {
        this.questionRepo = questionRepo;
        this.quizRepo = quizRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public QuestionDTO findById(int questionId) {
        return mapper.map(getQuestion(questionId), QuestionDTO.class);
    }

    @Override
    public Set<QuestionDTO> findAll() {
        return questionRepo.findAll().stream().map(question -> mapper.map(question, QuestionDTO.class)).collect(Collectors.toSet());
    }

    @Override
    public QuestionDTO saveQuestion(QuestionDTO question, int quizId) {
        Question dbQuestion = persistQuestion(question, quizId);
        return mapper.map(dbQuestion, QuestionDTO.class);
    }

    @Override
    public void deleteById(int questionId) {
        questionRepo.deleteById(questionId);
    }

    @Override
    public boolean existsByQuestionText(String text, int questionId) {
        boolean exists = questionRepo.existsByQuestion(text);
        if (questionId != 0) {
            Question byText = questionRepo.findByQuestion(text).orElse(null);
            if (byText == null) {
                return false;
            }
            Question byId = getQuestion(questionId);
            return exists && byId.getId() != byText.getId();
        }
        return exists;
    }

    @Override
    public List<QuestionStatistic> getQuestionsWithWorstAnswer() {
        List<Question> questions = questionRepo.findAll();
        List<QuestionStatistic> statistics = new ArrayList<>();
        for (Question question : questions) {
            QuestionStatistic statistic = new QuestionStatistic();
            statistic.setQuestionText(question.getQuestion());
            question.getAnswers().stream()
                    .filter(answer -> !answer.isCorrect())
                    .min(Comparator.comparing(Answer::getPercentage))
                    .map(answer -> {
                        statistic.setAnswerPercentage(answer.getPercentage());
                        statistic.setAnswerText(answer.getAnswer());
                        return answer;
                    });
            statistics.add(statistic);
        }
        return statistics;
    }

    @Override
    public List<QuestionDTO> findAllSortedByPercentageDesc() {
        return questionRepo.findAllByOrderByPercentageDesc().stream()
                .map(question -> mapper.map(question, QuestionDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public QuizScore updateQuizScore(QuestionDTO question, QuizScore quizScore) {

        quizScore.getUserScore().setNumberOfAnswers(quizScore.getUserScore().getNumberOfAnswers() + 1);
        question.setTakes(question.getTakes() + 1);

        List<AnswerDTO> answers = updateAnswers(question);

        if (isQuestionsAnswerCorrect(question)) {
            question.setCorrectlyAnsweredCounter(question.getCorrectlyAnsweredCounter() + 1);
            quizScore.getUserScore().setCorrectAnswers(quizScore.getUserScore().getCorrectAnswers() + 1);
        }

        question.setPercentage(calculatePercentage(question));
        question.setAnswers(answers);
        quizScore.setIndex(quizScore.getIndex() + 1);
        quizScore.getQuestions().stream()
                .filter(questionDTO -> questionDTO.getId() == question.getId())
                .forEach(questionDTO -> mapper.map(question, questionDTO));
        return quizScore;
    }

    @Override
    public QuizScore updateQuizScoreAfterCancel(QuestionDTO question, QuizScore quizScore) {
        quizScore = updateQuizScoreAfterSkip(question, quizScore);
        quizScore.getUserScore().setCanceled(true);
        return quizScore;
    }

    @Override
    public List<QuestionDTO> saveAll(List<QuestionDTO> questions) {
        List<Question> q = questionRepo.saveAll(questions.stream().map(question -> mapper.map(question, Question.class))
                .collect(Collectors.toList()));
        return q.stream()
                .map(question -> mapper.map(q, QuestionDTO.class))
                .collect(Collectors.toList());
    }

    private double calculatePercentage(QuestionDTO question) {
        if (question.getCorrectlyAnsweredCounter() == 0 || question.getTakes() == 0) {
            return 0.0;
        }
        return ((double) question.getCorrectlyAnsweredCounter() / question.getTakes()) * 100.0;
    }

    private double calculatePercentage(AnswerDTO answer) {
        if (answer.getCorrectlyAnsweredCounter() == 0 || answer.getTakes() == 0) {
            return 0.0;
        }
        return ((double) answer.getCorrectlyAnsweredCounter() / answer.getTakes()) * 100.0;
    }

    private List<AnswerDTO> updateAnswers(QuestionDTO question) {
        return question.getAnswers().stream()
                .peek(answer -> {
                    if ((answer.isCorrect() && answer.isUserAnswer()) || (!answer.isCorrect() && !answer.isUserAnswer())) {
                        answer.setCorrectlyAnsweredCounter(answer.getCorrectlyAnsweredCounter() + 1);
                        answer.setTakes(answer.getTakes() + 1);
                    }
                    if ((answer.isCorrect() && !answer.isUserAnswer()) || (!answer.isCorrect() && answer.isUserAnswer())) {
                        answer.setTakes(answer.getTakes() + 1);
                    }
                    answer.setPercentage(calculatePercentage(answer));
                })
                .collect(Collectors.toList());
    }

    private boolean isQuestionsAnswerCorrect(QuestionDTO question) {
        long countCorrect = question.getAnswers().stream()
                .filter(answer -> (answer.isCorrect() && answer.isUserAnswer())
                        || (!answer.isCorrect() && !answer.isUserAnswer()))
                .count();
        return countCorrect == question.getAnswers().size();
    }

    private Question getQuestion(String text) {
        return questionRepo.findByQuestion(text).orElseThrow(() -> new ResourceNotFoundException("Question not found"));
    }


    @Override
    public QuizScore updateQuizScoreAfterSkip(QuestionDTO question, QuizScore quizScore) {
        question.setTakes(question.getTakes() + 1);
        question.setPercentage(calculatePercentage(question));
        quizScore.getUserScore().setNumberOfAnswers(quizScore.getUserScore().getNumberOfAnswers() + 1);
        quizScore.setIndex(quizScore.getIndex() + 1);
        quizScore.getQuestions().stream()
                .filter(questionDTO -> questionDTO.getId() == question.getId())
                .forEach(questionDTO -> mapper.map(question, questionDTO));
        return quizScore;
    }

    private Question getQuestion(int id) {
        return questionRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Question not found"));
    }

    private Quiz getQuiz(int quizId) {
        return quizRepo.findById(quizId).orElseThrow(() -> new ResourceNotFoundException("Quiz not found"));
    }

    private Question persistQuestion(QuestionDTO question, int quizId) {
        Question persistedQuestion;
        Quiz quiz = getQuiz(quizId);
        if (question.getId() == 0) {
            question.setQuiz(quiz);
            persistedQuestion = questionRepo.save(mapper.map(question, Question.class));
        } else {
            Question dbQuestion = getQuestion(question.getId());
            mapper.map(question, dbQuestion);
            persistedQuestion = questionRepo.save(dbQuestion);
        }
        return persistedQuestion;
    }
}
