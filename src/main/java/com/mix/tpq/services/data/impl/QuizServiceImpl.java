package com.mix.tpq.services.data.impl;

import com.mix.tpq.models.entities.Question;
import com.mix.tpq.models.entities.Quiz;
import com.mix.tpq.models.entities.User;
import com.mix.tpq.models.payload.*;
import com.mix.tpq.repositories.QuizRepo;
import com.mix.tpq.services.data.QuizService;
import com.mix.tpq.services.exceptions.QuizExistsException;
import com.mix.tpq.services.exceptions.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuizServiceImpl implements QuizService {

    private final QuizRepo quizRepo;

    private ModelMapper mapper;

    public QuizServiceImpl(QuizRepo quizRepo) {
        this.quizRepo = quizRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public QuizDTO findById(int id) {
        Quiz quiz = getQuiz(id);
        return mapper.map(quiz, QuizDTO.class);
    }

    @Override
    public List<QuizDTO> findAll() {
        return quizRepo.findAll().stream().map(quiz -> mapper.map(quiz, QuizDTO.class)).collect(Collectors.toList());
    }

    @Override
    public QuizDTO saveQuiz(QuizDTO quiz) {
        if (isQuizExists() && (quiz.getId() != findAll().get(0).getId())) {
            throw new QuizExistsException("Quiz already exist, please remove existing one first");
        }
        Quiz persistedQuiz = persistQuiz(quiz);
        return mapper.map(persistedQuiz, QuizDTO.class);
    }

    @Override
    public void deleteQuiz(int id) {
        quizRepo.deleteById(id);
    }

    @Override
    public boolean isQuizExists() {
        return quizRepo.findAll().size() > 0;
    }

    private Quiz persistQuiz(QuizDTO quizDTO) {
        Quiz persistedQuiz;
        if (quizDTO.getId() == 0) {
            persistedQuiz = quizRepo.save(mapper.map(quizDTO, Quiz.class));
        } else {
            Quiz dbQuiz = getQuiz(quizDTO.getId());
            mapper.map(quizDTO, dbQuiz);
            persistedQuiz = quizRepo.save(dbQuiz);
        }
        return persistedQuiz;
    }

    private Quiz getQuiz(int id) {
        return quizRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Quiz doesnt exist"));
    }
}
