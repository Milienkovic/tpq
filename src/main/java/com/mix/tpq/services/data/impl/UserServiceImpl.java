package com.mix.tpq.services.data.impl;

import com.mix.tpq.models.entities.Role;
import com.mix.tpq.models.entities.User;
import com.mix.tpq.models.payload.RegisterUser;
import com.mix.tpq.models.payload.UserDTO;
import com.mix.tpq.models.payload.UserScore;
import com.mix.tpq.repositories.QuizRepo;
import com.mix.tpq.repositories.RoleRepo;
import com.mix.tpq.repositories.UserRepo;
import com.mix.tpq.services.data.UserService;
import com.mix.tpq.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final QuizRepo quizRepo;
    private ModelMapper mapper;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepo userRepo, RoleRepo roleRepo, QuizRepo quizRepo) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.quizRepo = quizRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO findById(int id) {
        User dbUser = userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        return mapper.map(dbUser, UserDTO.class);
    }

    @Override
    public UserDTO findByUsername(String username) {
        User dbUser = userRepo.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        return mapper.map(dbUser, UserDTO.class);
    }

    @Override
    public Set<UserDTO> findAll() {
        List<User> users = userRepo.findAll();
        return users.stream().filter(user -> user.getRole().getName().equals("USER"))
                .map(user -> mapper.map(user, UserDTO.class))
                .collect(Collectors.toSet());
    }

    @Override
    public UserDTO save(RegisterUser registerUser) {
        User user = new User();
        mapper.map(registerUser, user);
        Role role = roleRepo.findByName("USER");
        user.setRole(role);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return mapper.map(userRepo.save(user), UserDTO.class);
    }

    @Override
    public UserDTO update(UserScore userScore, String username) {
        User user = getUser(username);
        mapper.map(userScore, user);
        log.error("user " + user);
        return mapper.map(userRepo.save(user), UserDTO.class);
    }

    @Override
    public void deleteById(int id) {
        userRepo.deleteById(id);
    }

    @Override
    public List<UserDTO> findTop5ByCorrectAnswers() {
        return userRepo.findAll().stream()
                .sorted(Comparator.comparing(User::getCorrectAnswers).reversed())
                .limit(5)
                .map(user -> mapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> findAllSortedByScoreDesc() {
        System.out.println(userRepo.findAllByOrderByCorrectAnswersDesc());
        return userRepo.findAllByOrderByCorrectAnswersDesc().stream()
                .filter(user -> user.getRole().getName().equals("USER"))
                .map(user -> mapper.map(user, UserDTO.class)).collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> usersWithAllAnswers() {
        int size = getQuizSize();
        return userRepo.findAll().stream()
                .filter(user -> user.getNumberOfAnswers() == size && user.getRole().getName().equals("USER"))
                .map(user -> mapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> usersWithAllCorrectAnswers() {
        int size = getQuizSize();
        return userRepo.findAll().stream()
                .filter(user -> user.getCorrectAnswers() == size && user.getRole().getName().equals("USER"))
                .map(user -> mapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsByUsername(String username) {
        return userRepo.existsByUsername(username);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepo.existsByEmail(email);
    }

    @Override
    public boolean isTopPlayer(String username) {
        User u = getUser(username);
        User topUser = userRepo.findAll()
                .stream()
                .max(Comparator.comparing(User::getCorrectAnswers))
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));
        log.error(topUser+"");
        return u.getCorrectAnswers() >= topUser.getCorrectAnswers();
    }

    @Override
    public void updateAll() {
        UserScore score = new UserScore();
        List<User> users = userRepo.findAll();
        users.stream()
                .peek(user -> mapper.map(score,user))
                .collect(Collectors.toList());
        userRepo.saveAll(users);
    }


    private int getQuizSize() {
        return quizRepo.findAll().get(0).getQuestions().size();
    }

    private User getUser(String username) {
        return userRepo.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }
}
