package com.mix.tpq.services.exceptions;

public class QuizExistsException extends  RuntimeException {

    public QuizExistsException(String message) {
        super(message);
    }
}
