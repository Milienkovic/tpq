package com.mix.tpq.services.exceptions;

public class TooManyCorrectAnswersException extends RuntimeException {

    public TooManyCorrectAnswersException(String message) {
        super(message);
    }
}
