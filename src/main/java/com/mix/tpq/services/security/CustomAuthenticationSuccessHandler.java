package com.mix.tpq.services.security;

import com.mix.tpq.models.entities.User;
import com.mix.tpq.repositories.UserRepo;
import com.mix.tpq.services.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@Slf4j
@Service
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private final UserRepo userRepo;

    public CustomAuthenticationSuccessHandler(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        String targetUrl = determineTargetUrl(httpServletRequest, httpServletResponse, authentication);
        logger.error(targetUrl);
        updateLastVisited(authentication);
        getRedirectStrategy().sendRedirect(httpServletRequest, httpServletResponse, "/quiz");
        clearAuthenticationAttributes(httpServletRequest);
    }

    private void updateLastVisited(Authentication authentication){
        String username = authentication.getName();
        User user = userRepo.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        user.setLastVisited(new Timestamp(System.currentTimeMillis()));
        userRepo.save(user);
    }
}
