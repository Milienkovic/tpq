package com.mix.tpq.services.security;

import com.mix.tpq.models.entities.User;
import com.mix.tpq.models.payload.UserDTO;
import com.mix.tpq.repositories.UserRepo;
import com.mix.tpq.services.data.UserService;
import com.mix.tpq.services.exceptions.ResourceNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepo userRepo;

    public CustomUserDetailsService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username).orElseThrow(()-> new ResourceNotFoundException("user not found"));
        return new CustomUserDetails(user);
    }
}
