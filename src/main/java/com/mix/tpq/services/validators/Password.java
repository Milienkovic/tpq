package com.mix.tpq.services.validators;

import com.mix.tpq.services.validators.impl.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface Password {
    String message() default "Invalid Password, must contain at least 1 uppercase Character and a Digit 4-8 chars";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regex() default "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$";
}
