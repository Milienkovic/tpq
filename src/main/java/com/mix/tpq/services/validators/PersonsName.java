package com.mix.tpq.services.validators;

import com.mix.tpq.services.validators.impl.PersonsNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
@Constraint(validatedBy = PersonsNameValidator.class)
public @interface PersonsName {
    String message() default "Bad person's name";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regexp() default "^[A-Z][A-Za-z]{2,30}$";
}
