package com.mix.tpq.services.validators;

import com.mix.tpq.services.validators.impl.UsernameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = UsernameValidator.class)
public @interface Username {
    String message() default "Invalid username 4 to 10 alphanumerical chars";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regex() default "^[a-zA-Z0-9]+$";
}
