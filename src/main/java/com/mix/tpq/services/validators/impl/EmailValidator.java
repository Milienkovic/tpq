package com.mix.tpq.services.validators.impl;

import com.mix.tpq.services.validators.Email;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailValidator implements ConstraintValidator<Email, String> {
    private String regex = "";

    @Override
    public void initialize(Email constraintAnnotation) {
        regex = constraintAnnotation.regexp();
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        return email.matches(regex);
    }
}
