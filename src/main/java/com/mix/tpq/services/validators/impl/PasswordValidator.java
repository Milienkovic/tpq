package com.mix.tpq.services.validators.impl;

import com.mix.tpq.services.validators.Password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    private String regex = "";

    public void initialize(Password constraint) {
        regex = constraint.regex();
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.length() > 3 && value.length() < 9 && value.matches(regex);
    }
}
