package com.mix.tpq.services.validators.impl;

import com.mix.tpq.services.validators.PersonsName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PersonsNameValidator implements ConstraintValidator<PersonsName, String> {
    private String regex = "";

    public void initialize(PersonsName constraint) {
        regex = constraint.regexp();
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.matches(regex);
    }

}
