package com.mix.tpq.services.validators.impl;

import com.mix.tpq.services.validators.Username;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<Username, String> {
    private String regex = "";

    @Override
    public void initialize(Username constraintAnnotation) {
        regex = constraintAnnotation.regex();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return value.length() < 11 && value.length() > 3 && value.matches(regex);
    }
}
