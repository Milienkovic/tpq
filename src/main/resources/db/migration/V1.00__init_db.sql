CREATE TABLE `User` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`first_name` varchar(50),
	`last_name` varchar(50),
	`email` varchar(255) UNIQUE,
	`username` varchar(50) NOT NULL UNIQUE,
	`password` varchar(255) NOT NULL,
	`correct_answers` INT,
	`number_of_answers` INT,
	`canceled` BOOLEAN DEFAULT false,
	`last_visited` DATE,
	`role_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Answer` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`answer` varchar(255) NOT NULL UNIQUE,
	`correct` BOOLEAN NOT NULL DEFAULT false,
	`correctly_answered_counter` INT,
	`takes` INT,
	`percentage` FLOAT,
	`question_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Question` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`question` varchar(255) NOT NULL UNIQUE,
	`correctly_answered_counter` INT,
	`takes` INT,
	`percentage` FLOAT,
	`quiz_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Role` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(25) NOT NULL UNIQUE,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Quiz` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `User` ADD CONSTRAINT `User_fk0` FOREIGN KEY (`role_id`) REFERENCES `Role`(`id`);

ALTER TABLE `Answer` ADD CONSTRAINT `Answer_fk0` FOREIGN KEY (`question_id`) REFERENCES `Question`(`id`);

ALTER TABLE `Question` ADD CONSTRAINT `Question_fk0` FOREIGN KEY (`quiz_id`) REFERENCES `Quiz`(`id`);

